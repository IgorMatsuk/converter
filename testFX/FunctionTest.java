import main.Function;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunctionTest {

    @Test
    public void lenthConvert_firstNumToSecond_ok(){

        String convertFrom = "m";
        String convertTo = "km";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = true;
        String expected = "0,005";

        String actual = Function.lenthConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);

        assertEquals(expected, actual);
    }

    @Test
    public void lenthConvert_secondNumToFirst_ok(){

        String convertFrom = "m";
        String convertTo = "km";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = false;
        String expected = "5000";

        String actual = Function.lenthConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);

        assertEquals(expected, actual);
    }

    @Test
    public void temperatureConvert_firstNumToSecond_ok(){

        String convertFrom = "C";
        String convertTo = "F (Шкала Фаренгейта)";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = true;
        String expected = "41";

        String actual = Function.temperatureConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);

        assertEquals(expected, actual);
    }

    @Test
    public void temperatureConvert_secondNumToFirst_ok(){

        String convertFrom = "C";
        String convertTo = "F (Шкала Фаренгейта)";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = false;
        String expected = "-15";

        String actual = Function.temperatureConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);

        assertEquals(expected, actual);
    }

    @Test
    public void weightConvert_firstNumToSecond_ok(){

        String convertFrom = "kg";
        String convertTo = "g";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = true;
        String expected = "5000";

        String actual = Function.weightConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);

        assertEquals(expected, actual);
    }

    @Test
    public void weightConvert_secondNumToFirst_ok(){

        String convertFrom = "kg";
        String convertTo = "g";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = false;
        String expected = "0,005";

        String actual = Function.weightConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);

        assertEquals(expected, actual);
    }

    @Test
    public void timeConvert_firstNumToSecond_ok(){

        String convertFrom = "sec";
        String convertTo = "Hour";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = true;
        String expected = "0,001";

        String actual = Function.timeConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);

        assertEquals(expected, actual);
    }

    @Test
    public void timeConvert_secondNumToFirst_ok(){

        String convertFrom = "sec";
        String convertTo = "Hour";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = false;
        String expected = "18000";

        String actual = Function.timeConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);

        assertEquals(expected, actual);
    }

    @Test
    public void volumeConvert_firstNumToSecond_ok(){

        String convertFrom = "l";
        String convertTo = "gallon";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = true;
        String expected = "1,321";

        String actual = Function.volumeConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);

        assertEquals(expected, actual);
    }

    @Test
    public void volumeConvert_secondNumToFirst_ok(){

        String convertFrom = "l";
        String convertTo = "gallon";
        Double firstNum = 5.0;
        Double secondNum = 5.0;
        Boolean firstNumToSecond = false;
        String expected = "18,927";

        String actual = Function.volumeConvert(convertFrom, convertTo, firstNum, secondNum, firstNumToSecond);

        assertEquals(expected, actual);
    }
}

import main.Category;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CategoryTest {

    @Test
    public void getAllUnits_category_ok(){

        Category category = Category.LENGTH;
        ArrayList expected = new ArrayList (Arrays.asList("m", "km", "mile", "nautical mile", "cable", "league", "foot", "yard"));

        ArrayList actual = Category.getAllUnits(category);

        assertTrue(actual.contains("league"));
        assertTrue(Arrays.equals(new ArrayList[]{expected}, new ArrayList[]{actual}));
    }

    @Test
    public void getUnit0_category_ok(){

        Category category = Category.LENGTH;
        String expected = "m";

        String actual = Category.getUnit0(category);

        assertEquals(expected, actual);
    }

    @Test
    public void getUnit1_category_ok(){

        Category category = Category.LENGTH;
        String expected = "km";

        String actual = Category.getUnit1(category);

        assertEquals(expected, actual);
    }
}
